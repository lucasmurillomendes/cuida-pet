import 'package:cuidapet_mobile/app/core/ui/widgets/cuidapet_default_button.dart';
import 'package:cuidapet_mobile/app/core/ui/widgets/cuidapet_text_form_field.dart';
import 'package:flutter/material.dart';

class RegisterForm extends StatefulWidget {
  const RegisterForm({Key? key}) : super(key: key);

  @override
  State<RegisterForm> createState() => RegisterFormState();
}

class RegisterFormState extends State<RegisterForm> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 20,
        ),
        CuidapetTextFormField(
          label: 'Login',
        ),
        const SizedBox(
          height: 10,
        ),
        CuidapetTextFormField(
          label: 'Senha',
          obscureText: true,
        ),
        const SizedBox(
          height: 10,
        ),
        CuidapetTextFormField(
          label: 'Confirmar Senha',
          obscureText: true,
        ),
        const SizedBox(height: 20),
        CuidapetDefaultButton(
          label: 'Cadastrar',
          onPressed: () {},
        )
      ],
    );
  }
}
