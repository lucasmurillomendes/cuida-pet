class UserExistsException {
  final String? message;

  UserExistsException([
    this.message,
  ]);
}
