import 'package:cuidapet_mobile/app/core/helpers/enviroments.dart';
import 'package:cuidapet_mobile/app/core/push_notification/push_notification.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';

class ApplicationStartConfig {
  Future<void> configureApp() async {
    WidgetsFlutterBinding.ensureInitialized();
    await _firebaseConfig();
    await _loadEnvs();
    await _pushNotification();
  }

  Future<void> _firebaseConfig() => Firebase.initializeApp();

  Future<void> _loadEnvs() => Enviroments.loadEnvs();

  Future<void> _pushNotification() => PushNotification().configure();
}
